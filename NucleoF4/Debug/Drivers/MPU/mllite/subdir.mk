################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/MPU/mllite/data_builder.c \
../Drivers/MPU/mllite/hal_outputs.c \
../Drivers/MPU/mllite/message_layer.c \
../Drivers/MPU/mllite/ml_math_func.c \
../Drivers/MPU/mllite/mlmath.c \
../Drivers/MPU/mllite/mpl.c \
../Drivers/MPU/mllite/mpu_required_functions.c \
../Drivers/MPU/mllite/results_holder.c \
../Drivers/MPU/mllite/start_manager.c \
../Drivers/MPU/mllite/storage_manager.c 

OBJS += \
./Drivers/MPU/mllite/data_builder.o \
./Drivers/MPU/mllite/hal_outputs.o \
./Drivers/MPU/mllite/message_layer.o \
./Drivers/MPU/mllite/ml_math_func.o \
./Drivers/MPU/mllite/mlmath.o \
./Drivers/MPU/mllite/mpl.o \
./Drivers/MPU/mllite/mpu_required_functions.o \
./Drivers/MPU/mllite/results_holder.o \
./Drivers/MPU/mllite/start_manager.o \
./Drivers/MPU/mllite/storage_manager.o 

C_DEPS += \
./Drivers/MPU/mllite/data_builder.d \
./Drivers/MPU/mllite/hal_outputs.d \
./Drivers/MPU/mllite/message_layer.d \
./Drivers/MPU/mllite/ml_math_func.d \
./Drivers/MPU/mllite/mlmath.d \
./Drivers/MPU/mllite/mpl.d \
./Drivers/MPU/mllite/mpu_required_functions.d \
./Drivers/MPU/mllite/results_holder.d \
./Drivers/MPU/mllite/start_manager.d \
./Drivers/MPU/mllite/storage_manager.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/MPU/mllite/%.o: ../Drivers/MPU/mllite/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F401xE -DNUCLEO_F401RE -DMPU9250 -DEMPL -DUSE_DMP -I"/Users/michaelthompson/CortexM4/NucleoF4/Inc" -I"/Users/michaelthompson/CortexM4/NucleoF4/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Users/michaelthompson/CortexM4/NucleoF4/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Users/michaelthompson/CortexM4/NucleoF4/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Users/michaelthompson/CortexM4/NucleoF4/Drivers/CMSIS/Include" -I"/Users/michaelthompson/CortexM4/NucleoF4/Drivers/MPU/DMP" -I"/Users/michaelthompson/CortexM4/NucleoF4/Drivers/MPU/eMPL-hal" -I"/Users/michaelthompson/CortexM4/NucleoF4/Drivers/MPU/include" -I"/Users/michaelthompson/CortexM4/NucleoF4/Drivers/MPU/mllite" -I"/Users/michaelthompson/CortexM4/NucleoF4/Drivers/STM32F4xx_HAL_Driver/Inc"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


