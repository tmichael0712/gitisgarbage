################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/MPU/eMPL-hal/eMPL_outputs.c 

OBJS += \
./Drivers/MPU/eMPL-hal/eMPL_outputs.o 

C_DEPS += \
./Drivers/MPU/eMPL-hal/eMPL_outputs.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/MPU/eMPL-hal/%.o: ../Drivers/MPU/eMPL-hal/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F401xE -DNUCLEO_F401RE -DMPU9250 -DEMPL -DUSE_DMP -I"/Users/michaelthompson/CortexM4/NucleoF4/Inc" -I"/Users/michaelthompson/CortexM4/NucleoF4/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Users/michaelthompson/CortexM4/NucleoF4/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Users/michaelthompson/CortexM4/NucleoF4/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Users/michaelthompson/CortexM4/NucleoF4/Drivers/CMSIS/Include" -I"/Users/michaelthompson/CortexM4/NucleoF4/Drivers/MPU/DMP" -I"/Users/michaelthompson/CortexM4/NucleoF4/Drivers/MPU/eMPL-hal" -I"/Users/michaelthompson/CortexM4/NucleoF4/Drivers/MPU/include" -I"/Users/michaelthompson/CortexM4/NucleoF4/Drivers/MPU/mllite" -I"/Users/michaelthompson/CortexM4/NucleoF4/Drivers/STM32F4xx_HAL_Driver/Inc"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


