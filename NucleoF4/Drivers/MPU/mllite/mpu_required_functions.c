/*
 * mpu_required_functions.c
 *
 *  Created on: Jan 27, 2018
 *      Author: michaelthompson
 */


/*
 * mpu_required_functions.c
 *
 *  Created on: Jan 19, 2018
 *      Author: michaelthompson
 */

#include "mpu_required_functions.h"

#include "main.h"
#include "stm32f4xx_hal.h"

static unsigned char stm32f4xx_empl_i2c_wb[MAX_WRITE_SIZE];


int i2c_write_mpu(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char const *data)
{

	if (length + 1 > MAX_WRITE_SIZE)
		return -1;

	stm32f4xx_empl_i2c_wb[0] = reg_addr;
	memcpy(stm32f4xx_empl_i2c_wb + 1, data, length);

	while (__HAL_I2C_GET_FLAG(&hi2c3, I2C_FLAG_BUSY) == SET) ;

	HAL_I2C_Master_Transmit (&hi2c3, slave_addr << 1, stm32f4xx_empl_i2c_wb, length + 1, 1000);

	return 0;

}

int i2c_read_mpu(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char *data)
{
	while (__HAL_I2C_GET_FLAG(&hi2c3, I2C_FLAG_BUSY) == SET) ;

	HAL_I2C_Master_Transmit(&hi2c3, slave_addr << 1, &reg_addr, 1, 100);
	HAL_I2C_Master_Receive(&hi2c3, slave_addr << 1, data, length, 100);

	return 0;

}
void delay_ms_mpu(unsigned long num_ms)
{
	HAL_Delay(num_ms);
}
void get_ms_mpu(unsigned long *count)
{
	*count = (unsigned long)HAL_GetTick();
}
void reg_int_cb_mpu(struct int_param_s * intparam)
{

}

void logString(char * string)
{

}

void log_mpu(const char* fmt, ...)
{

}

void eMPL_send_quat(long *quat)
{

}

void eMPL_send_data(unsigned char type, long *data)
{
}


