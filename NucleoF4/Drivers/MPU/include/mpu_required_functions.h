/*
 * mpu_required_functions.h
 *
 *  Created on: Jan 27, 2018
 *      Author: michaelthompson
 */

#ifndef MPU_INCLUDE_MPU_REQUIRED_FUNCTIONS_H_
#define MPU_INCLUDE_MPU_REQUIRED_FUNCTIONS_H_

/*
 * mpu_required_functions.h
 *
 *  Created on: Jan 19, 2018
 *      Author: michaelthompson
 */

/* The following functions must be defined for this platform:
 * i2c_write(unsigned char slave_addr, unsigned char reg_addr,
 *      unsigned char length, unsigned char const *data)
 * i2c_read(unsigned char slave_addr, unsigned char reg_addr,
 *      unsigned char length, unsigned char *data)
 * delay_ms(unsigned long num_ms)
 * get_ms(unsigned long *count)
 * reg_int_cb(void (*cb)(void), unsigned char port, unsigned char pin)
 * labs(long x)
 * fabsf(float x)
 * min(int a, int b)
 */

#include "main.h"
#include "stm32f4xx_hal.h"

#include "inv_mpu.h"

I2C_HandleTypeDef hi2c3;

#define MAX_WRITE_SIZE     128

int i2c_write_mpu(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char const *data);
int i2c_read_mpu(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char *data);
void delay_ms_mpu(unsigned long num_ms);
void get_ms_mpu(unsigned long *count);
void reg_int_cb_mpu(struct int_param_s * intparam);
void log_mpu(const char* fmt, ...);

//int labs(long x);
//int min(int a, int b);




#endif /* MPU_INCLUDE_MPU_REQUIRED_FUNCTIONS_H_ */
